from django.test import TestCase

from django.urls import resolve, reverse

from pemesanan import views
from .views import form_pemesanan

from .models import Pemesanan
from django.utils import timezone


class PemesananTestCase(TestCase):
    def test_about_url_exists(self):
        response = self.client.get('/pemesanan/')
        self.assertEqual(response.status_code, 200)

    def test_about_using_about_template(self):
        response = self.client.get('/pemesanan/')
        self.assertTemplateUsed(response, 'pemesanan.html')
        
    def test_model_can_add_new_feedback(self):
        new_feedback = Pemesanan.objects.create(pickuptime=timezone.now(),returningtime=timezone.now(),locationpickup='Johar Baru',locationreturn='Johar Baru',name='Talitha', phonenumber = '081282395183', email='Ldmtalitha@gmail.com',additionalinfo='I am alone by myself')
        counting_all_feedback = Pemesanan.objects.all().count()
        self.assertEqual(counting_all_feedback,1)

    def test_can_save_a_POST_request(self):
        Pemesanan.objects.create(pickuptime=timezone.now(),returningtime=timezone.now(),locationpickup='Depok',locationreturn='Citayem',name='Andi', phonenumber = '08111981584', email='aa@gmail.com',additionalinfo='I am alone by myself')
        new_feedback = Pemesanan.objects.create(pickuptime=timezone.now(),returningtime=timezone.now(),locationpickup='Citayem',locationreturn='Bogor',name='Lala', phonenumber = '08111444605', email='lalaa@gmail.com',additionalinfo='I am alone by myself')
        response = self.client.post(reverse('pemesanan:add_pemesanan'),{'pickuptime':'2012-09-04 06:00','returningtime':'2012-09-04 06:00','locationpickup':'Depok','locationreturn':'Citayem','name':'Andi','phonenumber': '08111891584', 'email':'aa@gmail.com','additionalinfo':'I am alone by myself'})
        self.assertEqual(response.status_code, 200)

    def test_pemesanan_url_exists(self):
        response = self.client.get('/pemesanan/add_pemesanan/')
        self.assertEqual(response.status_code, 200)
        