from django.urls import path
from .views import *
#url for app

app_name = 'pemesanan'

urlpatterns = [
    path('', form_pemesanan, name = 'form_pemesanan'),
    path('add_pemesanan/', add_pemesanan, name = 'add_pemesanan'),
    path('api/rides/', api_rides, name='api_rides')
]
