from django.shortcuts import render, redirect
from .models import Pemesanan
from dtbsmotor.models import *
from django.http import JsonResponse, HttpResponse
from django.core import serializers

def form_pemesanan(request):
	rides = dtbs_motor.objects.all()
	return render(request, 'pemesanan.html', {'rides':rides})

def add_pemesanan(request):
	if (request.method == 'POST'):
		feedback=Pemesanan()
		feedback.pickuptime = request.POST.get('pickuptime')
		feedback.returningtime = request.POST.get('returningtime')
		feedback.locationpickup = request.POST.get('locationpickup')
		feedback.locationreturn = request.POST.get('locationreturn')
		feedback.name = request.POST.get('name')
		feedback.phonenumber= request.POST.get('phonenumber')
		feedback.additionalinfo= request.POST.get('additionalinfo')
		feedback.email = request.POST.get('email')
		feedback.ride = request.POST.get('rides')
		feedback.save()
		return render(request, 'feedback-pemesanan.html')
	else:
		return render(request, 'feedback-pemesanan.html')



def api_rides(request):
	kategori = request.POST.get('kategori', None)
	print(kategori)
	rides= dtbs_motor.objects.filter(category=kategori)
	data = serializers.serialize("json",rides)
	return HttpResponse(data, content_type='application/json')

	# 	pickuptime = request.POST['pickuptime']
	# 	returningtime = request.POST['returningtime']
	# 	locationpickup = request.POST['locationpickup']
	# 	locationreturn = request.POST['locationreturn']
	# 	name = request.POST['name']
	# 	phonenumber = request.POST['phonenumber']
	# 	email = request.POST['email']
	# 	additionalinfo = request.POST['additionalinfo']
	# 	pemesanan = Pemesanan(pickuptime = pickuptime, returningtime = returningtime, locationpickup = locationpickup, 
	# 	locationreturn = locationreturn, name = name, email = email, phonenumber = phonenumber, additionalinfo = additionalinfo)
	# 	pemesanan.save()
	# 	return render(request, 'feedback-pemesanan.html')
	# else:
	# 	return render (request, 'feedback-pemesanan.html')

# Create your views here.
