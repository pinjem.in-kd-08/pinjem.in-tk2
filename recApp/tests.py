from django.test import TestCase

from django.urls import resolve, reverse

from recApp import views
from .views import blog

from .models import MyModel
from django.utils import timezone


class AboutTestCase(TestCase):
    def test_about_url_exists(self):
        response = self.client.get('/blog/blog/')
        self.assertEqual(response.status_code, 200)


    def test_about_using_about_template(self):
        response = self.client.get('/blog/blog/')
        self.assertTemplateUsed(response, 'blog.html')

    def test_about_using_about_template(self):
        found = resolve(reverse('recApp:blog'))
        self.assertEqual(found.func, views.blog)

    def test_model_can_add_new_feedback(self):
        new_feedback = MyModel.objects.create(picture='https://d2pa5gi5n2e1an.cloudfront.net/global/images/product/motorcycle/Vespa_Sprint_150/Vespa_Sprint_150_L_1.jpg',judul='vespa',berita='IDR 300.000')
        counting_all_feedback = MyModel.objects.all().count()
        self.assertEqual(counting_all_feedback,1)

