from django.urls import path
from . import views
from recApp.views import blog


app_name = 'recApp'

urlpatterns = [
    path('blog/', views.blog, name='blog'),
    path('blog/data/', views.dataBlog, name='dataBlog')
    # path('', views)
]