from django import forms
from .models import MyModel
from django.utils.translation import ugettext_lazy as _

# class BlogForm(forms.ModelForm):
#     class Meta:
#         model = MyModel
#         fields = ['sorting']
#         labels = {
#             'sorting' : 'sorting'
#         }

# class BlogForm(forms.ModelForm):
#     class Meta:
#         model = MyModel
#         fields = ['created_at', 'update_at']
#         widgets = {
#             'created_at': forms.DateInput(attrs={'type': 'created_at'}),
#             'update_at': forms.DateInput(attrs={'type': 'update_at'}),
#             }
#         labels = {
#             'created_at':_('Created At'),
#             'update_at':_('Update At'),
#             }

#     def __init__(self, *args, **kwargs):
#         super().__init__(*args, **kwargs)
#         for field in self.Meta.fields:
#             self.fields[field].widget.attrs.update({
#                 'class': 'form-control'
#             })
