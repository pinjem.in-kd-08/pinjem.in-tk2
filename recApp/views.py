from django.shortcuts import render, redirect
from .models import MyModel
from django.http import JsonResponse
import json

def dataBlog(request):
    obj = MyModel.objects.all().values()    
    obj_list = list(obj)
    return JsonResponse(obj_list, safe = False)

def blog(request):
    obj = MyModel.objects.all()

    return render(request, 'blog.html', {'obj' : obj})



