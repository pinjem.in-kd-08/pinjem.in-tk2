from django.contrib.auth.models import User
from django.test import TestCase, Client
from django.urls import resolve, reverse
from oauth import views
from .views import SignIn

class AuthenticationTestCase(TestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.user = User.objects.create_user('audrey', 'audrey@gmail.com', 'audreycool')
        cls.user.first_name = 'audrey'
        cls.user.save()
        
    def test_Authentication_url_exists(self):
        response = self.client.get(reverse('oauth:SignIn'))
        self.assertEqual(response.status_code, 200)

    def test_Authentication_using_Authentication_func(self):
        found = resolve(reverse('oauth:SignIn'))
        self.assertEqual(found.func, views.SignIn)

    def test_Authentication_using_about_template(self):
        response = self.client.get(reverse('oauth:SignIn'))
        self.assertTemplateUsed(response, 'SignIn.html')
        self.assertContains(response, 'username')
        self.assertContains(response, 'password')

    def test_SignIn_in_is_redirect(self):
        self.client.login(username='audrey', password='audreycool')
        response = self.client.get(reverse('oauth:SignIn'))
        self.assertEqual(response.status_code, 302)
    
    def test_SignIn_not_SignedIn(self):
        response = self.client.get(reverse('oauth:SignIn'))
        html = response.content.decode()
        self.assertIn('<form', html)

    
    def test_SignIn_SignOut(self):
        response = self.client.post(
            reverse('oauth:SignIn'), data={
                'username': 'audrey',
                'password' : 'audreycool',
            }
        )

        self.client.get(reverse('oauth:SignOut'))
        response = self.client.get(reverse('homepage:homepage'))
        html = response.content.decode()
        self.assertNotIn(self.user.username, html)



