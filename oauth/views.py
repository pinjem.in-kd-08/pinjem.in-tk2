from django.shortcuts import render
from django.shortcuts import redirect
from django.contrib.auth import authenticate, login as auth_SignIn, logout as auth_SignOut

def SignIn(request):
    if request.user.is_authenticated:
        return redirect('homepage:homepage')
    else:
        if request.method == "POST":
            username = request.POST.get('username')
            password = request.POST.get('password')
            user = authenticate(request, username=username, password=password)
            if user is not None:
                auth_SignIn(request, user)
                request.session['username'] = user.first_name
                return redirect('homepage:homepage')
            else:
                return render(request, 'SignIn.html', {
                        'error' : 'invalid username or password'
                    }
                )
    return render(request, 'SignIn.html')

def SignOut(request):
    auth_SignOut(request)
    return redirect('homepage:homepage')