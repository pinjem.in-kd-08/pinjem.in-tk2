from django.urls import path
from . import views

app_name = "oauth"

urlpatterns = [
    path('SignIn/', views.SignIn, name='SignIn'),
    path('SignOut/', views.SignOut, name='SignOut')
]