[![pipeline status](https://gitlab.com/pinjem.in-kd-08/pinjem.in/badges/master/pipeline.svg)](https://gitlab.com/pinjem.in-kd-08/pinjem.in/commits/master)
[![coverage report](https://gitlab.com/pinjem.in-kd-08/pinjem.in-tk2/badges/master/coverage.svg)](https://gitlab.com/pinjem.in-kd-08/pinjem.in-tk2/commits/master)

# Pinjem.in #

## KD-08 ##
Nama anggota kelompok :
Talitha Luthfiyah Dhany Maheswari,
Audrey Annisa,
Habel Christiando Tobing,
Fadhil Ibrahim,
Muhammad Farhan Ghaffar

Website yang dibuat adalah website peminjaman/rental motor secara online bernama Pinjem.in dengan beragam jenis pilihan motor dan harga yang terjangkau. Website ini bisa bermanfaat untuk turis lokal maupun mancanegara yang sedang berlibur ke sebuah tempat dan memerlukan kendaraan roda 2 berpergian.

### Daftar fitur: ###
1. pengisian tanggal peminjaman/pengembalian dan jenis motor yang ingin digunakan
2. rekomendasi motor sesuai dengan pengisian di fitur no.1
3. form pemesanan rental motor
4. pilihan motor-motor yang dapat disewa dan bisa difilter dengan kategori/lokasi/harga tertentu.
5. penjelasan secara menyeluruh tentang pinjem.in dan juuga terdapat form feedback
6. blog artikel tentang otomotif
7. form untuk mengajukan motor yang bisa disewakan untuk pinjem.in

### [Live app on heroku](https://pinjem-in-tk2.herokuapp.com)