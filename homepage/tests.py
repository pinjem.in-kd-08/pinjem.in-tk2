from django.test import TestCase

from django.urls import resolve, reverse

from homepage import views
from .views import homepage


class HomeTestCase(TestCase):
    def test_url_exists(self):
        response = self.client.get('')
        self.assertEqual(response.status_code, 200)

    def test_template_exists(self):
        response = self.client.get('')
        self.assertTemplateUsed(response, 'homepage.html')
        
    

   
        
