from django.test import TestCase

from django.urls import resolve, reverse

from about import views
from .views import about

from .models import add_feedback
from django.utils import timezone


class AboutTestCase(TestCase):
    def test_about_url_exists(self):
        response = self.client.get('/about/')
        self.assertEqual(response.status_code, 200)

    def test_about_using_about_func(self):
        found = resolve(reverse('about:about'))
        self.assertEqual(found.func, views.about)

    def test_about_using_about_template(self):
        response = self.client.get('/about/')
        self.assertTemplateUsed(response, 'about.html')

        
    def test_model_can_add_new_feedback(self):
        new_feedback = add_feedback.objects.create(name='audreyd',email='audreydatau@gmail.com',time=timezone.now(),motorcycle_type='automatic',message='terlalu murah')
        counting_all_feedback = add_feedback.objects.all().count()
        self.assertEqual(counting_all_feedback,1)
        
    def test_can_save_a_POST_request(self):
        add_feedback.objects.create(name='annisa',email='annisa@ui.ac.id',time=timezone.now(),motorcycle_type='manual',message='terlalu mahal')   
        new_feedback = add_feedback.objects.create(name='budi',email='budi@gmail.com',time=timezone.now(),motorcycle_type='automatic',message='keren')
        response = self.client.post(reverse('about:about'),{'name':'annisa','email':'annisa@ui.ac.id','time':'2012-09-04 06:00','motorcycle_type':'automatic','message':'terlalu mahal'})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(str(new_feedback),new_feedback.name)

    def test_json_using_about_func(self):
        found = resolve(reverse('about:motorcycle'))
        self.assertEqual(found.func, views.motorcycle)


