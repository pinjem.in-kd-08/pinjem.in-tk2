from django.db import models
from django.utils import timezone
from datetime import datetime, date

class add_feedback(models.Model):
    name = models.CharField(max_length=100)
    email = models.EmailField(max_length=100)
    time = models.DateTimeField(default=timezone.now)
    motorcycle_type = models.CharField(max_length=100)
    message = models.CharField(max_length=400)

    def __str__(self):
        return self.name

