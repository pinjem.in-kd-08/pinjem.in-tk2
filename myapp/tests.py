from django.test import TestCase

from django.urls import resolve, reverse

from myapp import views
from .views import contact

from .models import Snippet



class SnippetTestCase(TestCase):
    def test_url_exists(self):
        response = self.client.get('/rent/')
        self.assertEqual(response.status_code, 200)

    def test_template_exists(self):
        response = self.client.get('/rent/')
        self.assertTemplateUsed(response, 'form.html')
        
    def test_model_can_add_new_feedback(self):
        new_rent = Snippet.objects.create( name = "Habel",
                                                contact = "0888888888",
                                                email = "gampangbanget@yahoo.com",
                                                tipe = "Matic",
                                                condition = "Brand New")
        count_rent = Snippet.objects.all().count()
        self.assertEqual(count_rent,1)

   
        
