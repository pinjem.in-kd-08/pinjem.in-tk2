function response(){
    document.getElementById("response").innerHTML = "Thank you for your feedback!";
}

function changecolor(){
    document.getElementById("tombol").style.backgroundColor="red";
}

function normal(){
    document.getElementById("tombol").style.backgroundColor="blue";
}

$(document).ready(function(){

    load_json_data('motorcycle_type');

    function load_json_data(id)
    {
        var html_code = '';
        $.getJSON('motorcycle_type/',function(data){
            html_code += '<option value="">Select '+id+'</option>';
            $.each(data, function(key,value){
                if(id=='motorcycle_type')
                {
                    if(value.parent_id == "0")
                    {
                        html_code += '<option value="'+value.id+'">'+value.name+'</option>';
                    }
                }
            });
            $('#'+id).html(html_code);
        })
    }
});