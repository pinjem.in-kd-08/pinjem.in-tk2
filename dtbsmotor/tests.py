from django.test import TestCase

from django.urls import resolve, reverse

from dtbsmotor import views
from .views import dtbsmotor

from .models import dtbs_motor
from django.utils import timezone

# Create your tests here.
class ChoicesTestCase(TestCase):
    def test_about_url_exists(self):
        response = self.client.get('/choices/')
        self.assertEqual(response.status_code, 200)

    def test_about_using_about_func(self):
        found = resolve(reverse('dtbsmotor:dtbsmotor'))
        self.assertEqual(found.func, views.dtbsmotor)

    def test_about_using_about_template(self):
        response = self.client.get('/choices/')
        self.assertTemplateUsed(response, 'dtbsmotor.html')

        
    def test_model_can_add_new_feedback(self):
        new_feedback = dtbs_motor.objects.create(picture='4.png',motorcycle_type='vespa',price='IDR 300.000',location='East Jakarta', category='a')
        counting_all_feedback = dtbs_motor.objects.all().count()
        self.assertEqual(counting_all_feedback,1)
        
  
