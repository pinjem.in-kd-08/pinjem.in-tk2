from django.urls import path, include

from . import views

app_name = 'dtbsmotor'


urlpatterns = [
    path('', views.dtbsmotor, name='dtbsmotor'),   
    path('jsonreq/', views.jsonreq_func, name='jsonreq_func'),
]