from django.shortcuts import render,redirect
from .models import dtbs_motor
from . import forms
from .forms import MotorForm

def dtbsmotor(request):
    if request.method == 'POST':
        form = forms.MotorForm(request.POST)
        if form.is_valid():
            kategori = form.cleaned_data['category']
            obj = dtbs_motor.objects.filter(category = kategori)
            return render(request,'dtbsmotor.html',{
                'obj':obj,
                'form':form
                })
    else:
        form = MotorForm()
        return render(request, 'dtbsmotor.html',  {'form':form})


def jsonreq_func(request):
    return render(request, 'city.json')